<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Aceita apenas requisiçoes do tipo get e post
 */
Route::match(['get', 'post'], 'foo', function () {
    return "swewew";
});

/**
 * Rota de grupo com o filtro web
 * @var [type]
 */
Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('foo', function () {
        return "Este metodo GET";
    });

    Route::get('foo/bar', function() {
        return view('foo/bar', ['foo'=>'teste foo', 'bar'=>'123']);
    });

    Route::group(['prefix' => 'user'], function(){
    	Route::get('/', ['uses' => 'UserController@index']);
    	Route::post('add', ['uses' => 'UserController@add']);
    	Route::get('{id}', ['uses' => 'UserController@show']);


    });
});
