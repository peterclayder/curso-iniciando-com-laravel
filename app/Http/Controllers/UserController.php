<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    public function index(){
        return view('foo/bar', ['foo'=>'teste foo', 'bar'=>'123']);
    }

    public function show($id){
        echo $id;
    }

    public function add(Request $request){
        $this->validate($request,[
            'nome' => ['required'],
            'email' => ['required', 'email']
        ]);

    }
}
