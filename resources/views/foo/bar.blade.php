<!-- carrega a view base -->
@extends('foo.base')

<!-- Atribui o valor Bar page na variável title, lá na view base-->
@section('title', 'Bar page')

@section('container')
    <h2> Bar </h2>
    {{$errors->first('nome')}}

    <form action="/user/add" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        <input type="text" name="nome" placeholder="Nome">
        <input type="email" name="email" placeholder="Email" >
        <input type="submit" value="Enviar">
    </form>
@endsection
